$(document).ready(function(){

	//RESPOSANVEL POR INICIALIZAR A LISTA DE CONTATOS
	iniciarListaContatos();

	salvar = true;

	$('#telefone').mask('(00) 0000-00009');
	$('#telefone').blur(function(event) {
	   if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	      $('#telefone').mask('(00) 00000-0009');
	   } else {
	      $('#telefone').mask('(00) 0000-00009');
	   }
	});

	$('#telefone1').mask('(00) 0000-00009');
	$('#telefone1').blur(function(event) {
	   if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	      $('#telefone1').mask('(00) 00000-0009');
	   } else {
	      $('#telefone1').mask('(00) 0000-00009');
	   }
	});



	function iniciarListaContatos(){
		$.ajax({
				url: "ajax.php",
				type: "POST",
				data: {method:'ajaxCarregaListaContatos'} ,
				success: function(lista){
					lista = JSON.parse(lista);
					html = '';
		        	$.each(lista,function(e,contato){
		        		html += "\
		        		<tr class='linha-contato'>\
		        			<td>\
	                        	<span class='name'>Nome :"+contato.nome_contato+"</span>\
	                        	<span style='display:"+(contato.telefone == null ? 'none' : 'block' )+"' class='fone'>Telefones: <a href='tel:"+contato.telefone+"'> "+contato.telefone+"</a></span>\
	                        	<span style='display:"+(contato.email == null ? 'none' : 'block' )+"' class='email'>Email :<a href='mail:"+contato.email+"'> "+contato.email+"</a></span>\
	                        </td>\
	                        <td>\
	                        	<span class='editar-contato' id='"+contato.id+"'><i class='glyphicon glyphicon-pencil' style='font-size: 20px;'></i></span>\
	                        	<span class='excluir-contato' id='"+contato.id+"'><i class='glyphicon glyphicon-trash' style='font-size: 20px;'></i></span>\
	                    	</td>\
	                    </tr>";
		        	});

		        	if(html == ''){
		        		html = 'Nenhum contato cadastro no momento!';
		        	}

		        	$('.lista-contatos').append(html);
		    	}
		});		
	}

	function validaEmail(email){
		var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!filter.test(email)){
			alert('Por favor, digite o email corretamente');
			document.getElementById("email").focus();
			salvar = false;
		}else{
			salvar = true;
		}
	}



	$(document).on('click','.salvar-agenda',function(){
		
		id_agenda 		= $('#id_agenda').val();
		nome 			= $('#nome_contato').val(); 
		observacao 		= $('#observacao').val();


		if(nome.trim() == ''){
			alert("Necessário informar o Nome do Contato!");
			return false;
		}

		// mapeais os array de telefone da tela
		telefones = $("input[name='telefone[]']")
              .map(function(){
              	if($(this).val().length == 14 || $(this).val().length == 15){
              		salvar = true;
              		return $(this).val();
              	}else{
              		alert('Existe números invalidos!');
              		salvar = false;
              	}
              }).get();

        // mapeais os array de email da tela      
        emails = $("input[name='email[]']")
              .map(function(){
              	if($(this).val() != ''){
	              	validaEmail($(this).val());
	              	return $(this).val();
              	}
              }).get();      

       	if(salvar == false){
       		return false;
       	}       

		$.ajax({
			type: "POST",
			url: "ajax.php",
			data: {
					method:'ajaxSalvarDados',
					id_agenda:id_agenda,
					nome_contato:nome,
					observacao:observacao,
					telefones:telefones,
					emails:emails
				  }, 
			cache: false,
			success: function (data) {
				$('.lista-contatos').html('');
				iniciarListaContatos();
				$('#modal-agenda').modal('hide');
				$('#form_agenda')[0].reset();
				alert("Contato Salvo com sucesso!");
			},
			error: function (e) {
				console.log("ERROR : ", e);
			}
		});	
		
	});
});

$(document).on('click','.editar-contato',function(){
	id_agenda 	= $(this).attr('id');

		$.ajax({
			type: "POST",
			url: "ajax.php",
			data: {
					method:'ajaxGetDadosContato',
					id_agenda:id_agenda,
				  }, 
			cache: false,
			success: function (retorno) {
				retorno = JSON.parse(retorno);
				$('#id_agenda').val(retorno.id); 

				$('#nome_contato').val(retorno.nome_contato); 
				$('#observacao').val(retorno.observacao);	

				$('#telefone').val(retorno.telefone); 
				$('#email').val(retorno.email);	

				// MOSTRA A MODAL COM DADOS APOS A BUSCA DE DADOS
				$('#modal-agenda').modal('show');
			},
			error: function (e) {
				console.log("ERROR : ", e);
			}
		});		
	
});

$(document).on('click','.excluir-contato',function(){
	id_agenda 	= $(this).attr('id');
	if(confirm("Deseja realmente excluir esse contato?")){
		$.ajax({
			type: "POST",
			url: "ajax.php",
			data: {
					method:'ajaxExcluirContatoById',
					id_agenda:id_agenda,
				  }, 
			cache: false,
			success: function (retorno) {
				alert("Contato Excluir com sucesso!");
				window.location.reload();
			},
			error: function (e) {
				console.log("ERROR : ", e);
			}
		});		
	}

});



