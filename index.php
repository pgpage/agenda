<?php    
session_start();
include ('variaveis.php');
    ini_set("display_errors", "on");


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" >
    <title>Agenda Telefônica</title>


    <!-- jQuery -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/jquery/dist/jquery.min.js"></script>
    
    
    <script src="<?=CAMINHO_SISTEMA?>js/mask.min.js" type="text/javascript"></script>

      <!-- Script do sistema -->
    <script src="<?=CAMINHO_SISTEMA?>js/script.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/dist/js/sb-admin-2.js"></script>

    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=CAMINHO_SISTEMA?>js/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>
<style type="text/css">
    .excluir-contato,.editar-contato{
        padding-left: 15px;
        cursor: pointer;
    }
    .center{
        text-align: center;
    }
    .minha-lista{
        margin-top: 20px;
    }
    .linha-contato{
        list-style: none;
        padding: 5px;
        border: 1px solid #000;
    }
    .container{
        text-align: center;
    }
    .lista-contatos tr td{
        padding: 10px;
        text-align: left;
    }
    table{
        margin: 0px auto;
    }
</style>
<body>
    <div class="container">
        <div class="row ">
            <div class="col-md-12 col-sm-12">
                <h3 class="center">Minha Agenda</h3>
            </div>
            <div class="col-md-12 col-sm-12 novo-contato center"> 
                <input type="button" class="btn btn-success" value="Novo Contato" data-toggle="modal" data-target="#modal-agenda">
            </div>           
        </div>
        <div class="row minha-lista">
            <div class="col-md-12 col-sm-12">
                <table class="lista-contatos table table-striped table-bordered table-hover">
                   
               </table>
            </div> 
        </div>
    </div>
    <div id="modal-agenda" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Contato</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <form id="form_agenda">
                        <div class="form-group">
                            <label for="nome_contato">Nome Contato *</label>
                            <input id="id_agenda" type="hidden">
                            <input class="form-control" id="nome_contato" name="nome_contato" value="">                                           
                        </div>
                        <div class="form-group">
                            <label for="nome_contato">Telefone *</label>
                            <input class="form-control" id="telefone"  id="telefone" class="telefone" name="telefone[]" value=""  pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" > 
                        </div>  
                        <div class="form-group">
                            <label for="nome_contato">E-mail</label>
                            <input type="mail" class="form-control" id="email" class="email" name="email[]" value="">                                           
                        </div>                                       
                        <div class="form-group">
                            <label for="nome_contato">Observação</label>
                            <textarea class="form-control" id="observacao" name="observacao"></textarea>                                          
                        </div>                    
                        </div>                        
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary salvar-agenda">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
            </div>
        </div>
    </div>
</body>
</html>
