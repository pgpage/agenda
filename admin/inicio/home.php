<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Admin - Agenda v1.0</a>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <h6 class="dadosIntegrado">Usúario : <b>Agenda</b></h6>
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">

                    <li><a href="<?php echo CAMINHO_SISTEMA ?>admin/logout"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
           <!-- INCLUIR O MENU AQUI --> 
           <?php
           include("menu.php");
           ?>
       </div>
       <!-- /.navbar-static-side -->
   </nav>
   <div id="page-wrapper">
    <?php 
        if($rotas[1]){
            switch ($rotas[1]) {   
                case 'logout':
                    unset($_SESSION['login']);
                    echo "<script>location.href= ''; </script>";    
                break;
                case 'relatorio_de_contato':
                    require_once('../controller/agenda_admin.php');
                    $agenda_admin       = new agendaAdmin();
                    $listagem   = $agenda_admin->getListarContatos();

                    include('relatorio_contatos.php');   
                break;
                default:
                    require_once('../controller/agenda_admin.php');
                    $agenda_admin       = new agendaAdmin();
                    $ultimosCadastros   = $agenda_admin->getUltimosCadastros();
                    $totalCadastros     = $agenda_admin->getTotalCadastros();
                    $mesAnterior        = $agenda_admin->getQtdCadastrosMesAnterior();
                    $mesAtual           = $agenda_admin->getQtdCadastrosMesAtual();
                    include('dashbord.php'); 
                break;
            }
        } 
    ?>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>