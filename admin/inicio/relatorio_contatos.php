<style type="text/css">
.btnRight{
    float: right;
    padding-right: 20px;
}
.btnInserir a{
    color:#fff;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Relatório de Contatos
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                Relatório de Contatos
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body table-responsive" >
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover " id="dataTables-example">
                        <thead>
                            <tr>

                                <th>Id</th>
                                <th>Nome</th> 
                                <th>Telefone</th>
                                <th>E-mail</th> 
                                <th>Observação</th>   
                            </tr>
                        </thead>
                        <tbody>
                           <?php 
                           if(count($listagem) > 0){
                               for ($i=0; $i < count($listagem); $i++) { 
                                 ?>
                                 <tr class="odd gradeX">
                                    <td><?=$listagem[$i]['id']?></td> 
                                    <td><?=$listagem[$i]['nome_contato']?></td>
                                    <td><?=$listagem[$i]['telefone']?></td>   
                                    <td><?=$listagem[$i]['email']?></td> 
                                    <td><?=$listagem[$i]['observacao']?></td>         
                                </td>
                            </tr>
                            <?php }}else{ ?>
                            <tr>
                                <td colspan="8">
                                    Nenhum Registro encontrado!
                                </td>
                            </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>