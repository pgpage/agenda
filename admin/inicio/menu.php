<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li>
            <a href="<?=CAMINHO_ADMIN?>index"><i class="fa  fa-power-off fa-fw"></i> Inicio</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-file-o fa-fw"></i> Relatórios<span class="fa arrow"></span></a> 
            <ul class="nav nav-second-level">
                 <li>
                    <a href="<?=CAMINHO_ADMIN?>relatorio_de_contato">Relatório de Contatos</a>
                </li>               
            </ul>
        </li>                       
        <li></li>
    </ul>
</div>