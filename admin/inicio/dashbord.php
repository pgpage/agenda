<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Início</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <p>Bem Vindo ao Painel do Admin </p>
</div>
<div class="row">	
	<div class="col-sm-4 col-md-4" style="padding: 10px;">
		<span>Quantidade de Cadastro no mês Atual :</span>
		<span class="quantidade_mes_atual"><?=count($mesAtual['qtd']) > 0 ? $mesAtual['qtd'] : 0 ?></span>
	</div>		
	<div class="col-sm-4 col-md-4" style="padding: 10px;">
		<span>Quantidade de Cadastro no mês Anterior :</span>
		<span class="quantidade_mes_anterior"><?=count($mesAnterior['qtd']) > 0 ? $mesAnterior['qtd'] : 0 ?></span>
	</div>	
	<div class="col-sm-4 col-md-4" style="padding: 10px;">
		<span>Total Cadastros :</span>
		<span class="total_cadastros"><?=count($totalCadastros['qtd']) > 0 ? $totalCadastros['qtd'] : 0 ?> </span>
	</div>		
	<div class="col-sm-6 col-md-6" style="padding: 10px;">
		<span class="ultimo">Últimos Cadastros</span>
		<ul class="utlimos_cadastros">
			<?php foreach ($ultimosCadastros as $key => $value) { ?>
					<li><?=$value['nome_contato']?> - Data <?=$value['data_cadastro']?> </li>
			<?php }?>
		</ul>
	</div>
</div>