<?php    
session_start();
include ('variaveis.php');

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" >
    <title>Admin - Agenda</title>
    <!-- jQuery -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=CAMINHO_SISTEMA?>js/dist/js/sb-admin-2.js"></script>

    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=CAMINHO_SISTEMA?>js/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=CAMINHO_SISTEMA?>js/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>
<body>
	<?php 
        if(isset($_SESSION['login']) && $_SESSION['login']['logado'] == 1){
            include("inicio/home.php");  
        }else{
            include("login/login.php"); 
        }
    ?>
</body>
</html>
