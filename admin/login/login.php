<?php
    if($_POST){

        require_once('../controller/login.php');
        $login = new login();
     
        $login->validaAcesso($_POST['email'],$_POST['password']);

    }
?>
<style>
.alert-login{
    display:none;
    text-align: center;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Logue-se</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="<?=CAMINHO_ADMIN?>" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Usúario" name="email" type="text" autofocus required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="" required>
                            </div>

                            <button type="submit"  class="btn btn-lg btn-success btn-block">Login</button>                    
                        </fieldset>
                    </form>
                </div>
                <div id="alert-login" class="alert-login alert alert-danger">
                  E-mail ou Password estão incorretos!                        
              </div>
          </div>
      </div>
  </div>
</div>
