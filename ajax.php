<?php
require_once('controller/agenda.php');

$method = $_POST['method'];

// METODO RESPONSAVEL POR LISTAR OS DADOS DA AGENDA
if($method == 'ajaxCarregaListaContatos'){
    
    $agenda 	= new agenda();
   	$contatos  	= $agenda->listarContatos();
    echo json_encode($contatos);

}
// METODO RESPONSAVEL POR SALVAR OS DADOS DA AGENDA
else if($method == 'ajaxSalvarDados'){
	$dados  = $_POST;
	$agenda 	= new agenda();
	$retorno  	= $agenda->salvarContato($dados);

	echo json_encode($retorno);
}
// METODO RESPONSAVEL POR BUSCAR UM CONTATO PARA EDICAO POR ID
else if($method == 'ajaxGetDadosContato'){
	$id_agenda  = $_POST['id_agenda'];
	$agenda 	= new agenda();
	$retorno  	= $agenda->getDadosContatoById($id_agenda);
	
	echo json_encode($retorno);	
}
else if($method == 'ajaxExcluirContatoById'){
	$id_agenda  = $_POST['id_agenda'];
	$agenda 	= new agenda();
	$retorno  	= $agenda->excluirContatoById($id_agenda);
	
	echo json_encode($retorno);	
}

?>