/*	
	TABELAS A SEREM INSERIDAS AO CRIAR O PROJETO	
*/


create table agenda(
   id INT NOT NULL AUTO_INCREMENT,
   nome_contato VARCHAR(255) NOT NULL,
   observacao   VARCHAR(255),
   data_cadastro datetime,
   PRIMARY KEY ( id )
);


create table agenda_telefone(
   id INT NOT NULL AUTO_INCREMENT,
   id_agenda INT, 
   telefone VARCHAR(20) NOT NULL,
   data_cadastro datetime,
   PRIMARY KEY ( id ),
   FOREIGN KEY (id_agenda) REFERENCES agenda(id)
);

create table agenda_email(
   id INT NOT NULL AUTO_INCREMENT,
   id_agenda INT, 
   email VARCHAR(100) NOT NULL,
   data_cadastro datetime,
   PRIMARY KEY ( id ),
   FOREIGN KEY (id_agenda) REFERENCES agenda(id)
);

ALTER TABLE agenda MODIFY data_cadastro datetime DEFAULT NOW();


ALTER TABLE agenda_telefone MODIFY data_cadastro datetime DEFAULT NOW();


ALTER TABLE agenda_email MODIFY data_cadastro datetime DEFAULT NOW();




