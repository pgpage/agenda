<?php

require_once('../model/agenda_adminBd.php');

class AgendaAdmin extends AgendaAdminBd{

	
	function getListarContatos(){
		return $this->getListarContatosBd();
	}

	function getUltimosCadastros(){
		return $this->getUltimosCadastrosBd();
	}

	function getTotalCadastros(){
		return $this->getTotalCadastrosBd();
	}	

	function getQtdCadastrosMesAnterior(){
		return $this->getQtdCadastrosMesAnteriorBd();
	}

	function getQtdCadastrosMesAtual(){
		return $this->getQtdCadastrosMesAtualBd();
	}

}
?>
