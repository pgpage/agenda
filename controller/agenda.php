<?php

require_once('model/agendaBd.php');

class Agenda extends AgendaBd{

	function listarContatos(){
		return $this->listarContatosBD();
	}	

	function salvarContato($dados){
		return $this->salvarContatoBd($dados);
	}

	function getDadosContatoById($id_contato){
		return $this->getDadosContatoByIdBd($id_contato);
	}


	function excluirContatoById($id_contato){
		return $this->excluirContatoByIdBd($id_contato);
	}
}
?>
