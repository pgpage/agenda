<?php

require_once('../model/loginBd.php');

class Login extends LoginBd{

	/** 	
		* Função valida Acesso a aplicação do administrador
		* @access public 
		* @param String $password  
		* @param String $diretorio 
		* @return true or false  
	*/ 
	function validaAcesso($email , $password){          
		$dados = LoginBd::validaAcesso($email , $password);
		return $dados;
	}
}

?>
