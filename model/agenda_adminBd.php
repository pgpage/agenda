<?php

require_once('../model/conexao/conexao.php');

class AgendaAdminBd extends Conexao {

	

	function getListarContatosBd(){
		$sql = "SELECT a.id,a.nome_contato,agt.telefone,agm.email,a.observacao FROM agenda  a
				LEFT JOIN agenda_telefone agt on agt.id_agenda = a.id
				LEFT JOIN agenda_email agm on agm.id_agenda = a.id
				ORDER BY a.nome_contato asc";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	function getUltimosCadastrosBd(){
		$sql = "SELECT a.id,a.nome_contato,date_format(a.data_cadastro,'%d/%m/%Y %H:%i:%s ') as data_cadastro FROM agenda  a
				LEFT JOIN agenda_telefone agt on agt.id_agenda = a.id
				LEFT JOIN agenda_email agm on agm.id_agenda = a.id
				ORDER BY a.data_cadastro desc limit 5";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}



	function getTotalCadastrosBd(){
		$sql = "SELECT count(id) as qtd FROM agenda";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}


	function getQtdCadastrosMesAnteriorBd(){
		$sql = "SELECT count(id) AS qtd 
				FROM agenda
				WHERE MONTH(data_cadastro) = (MONTH(now()) - 1)";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}


	function getQtdCadastrosMesAtualBd(){
		$sql = "SELECT count(id) AS qtd 
				FROM agenda
				WHERE MONTH(data_cadastro) = MONTH(now())";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}


}


?>
