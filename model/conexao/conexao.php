<?php

class Conexao{

    public $conn;
    
    public function Conexao(){
        if($_SERVER['HTTP_HOST'] == 'localhost:8000'){
            define( 'MYSQL_HOST', 'localhost' );
            define( 'MYSQL_USER', 'root' );
            define( 'MYSQL_PASSWORD', 'root' );
            define( 'MYSQL_DB_NAME', 'agenda_local' );
        }else{
            define( 'MYSQL_HOST', 'localhost' );
            define( 'MYSQL_USER', 'agenda_producao' );
            define( 'MYSQL_PASSWORD', 'agenda_producao' );
            define( 'MYSQL_DB_NAME', 'agenda_producao' );       
        }

        try {                
            $this->conn  = new PDO( 'mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASSWORD );
        } catch (Exception $e) {
            echo $e->getMessage();
        }                    
    }

    public function trataErro(){
        if ($stmt->erroCode() == '00000'){
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }else{
            return $stmt->erroInfo()[2];
        }
    }

}

?>
