<?php

require_once('model/conexao/conexao.php');

class AgendaBd extends Conexao {

	function salvarContatoBd($dados){

		$id 		= $dados['id_agenda'];
		$telefones 	= $dados['telefones'];
		$emails 	= $dados['emails'];	

		if(isset($id) && $id != ''){
			$sql = "UPDATE agenda SET nome_contato = '".$dados['nome_contato']."',observacao = '".$dados['observacao']."' where id = ".$id." ;";
			$this->conn->exec($sql);
		}else{
			$sql = "INSERT INTO agenda (nome_contato,observacao,data_cadastro) VALUES ('".$dados['nome_contato']."','".$dados['observacao']."',now());";
			$this->conn->exec($sql);
			$id = $this->conn->lastInsertId();	
		}



		if(is_array($telefones) && count($telefones) > 0){

			$sql = "delete FROM agenda_telefone where id_agenda = ".$id;
			$this->conn->query($sql);
			
			foreach ($telefones as $key => $telefone) {
				$sql = "INSERT INTO agenda_telefone (id_agenda,telefone) VALUES (".$id.",'".$telefone."');";
				$this->conn->exec($sql);
			}
		}
		if(is_array($emails) && count($emails) > 0){
			
			$sql = "delete FROM agenda_email where id_agenda = ".$id;
			$this->conn->query($sql);

			foreach ($emails as $key => $email) {
				$sql = "INSERT INTO agenda_email (id_agenda,email) VALUES (".$id.",'".$email."');";

				$this->conn->exec($sql);
			}
		}

	}

	function listarContatosBD(){
		$sql = "SELECT a.id,a.nome_contato,agt.telefone,agm.email FROM agenda  a
				LEFT JOIN agenda_telefone agt on agt.id_agenda = a.id
				LEFT JOIN agenda_email agm on agm.id_agenda = a.id
				ORDER BY a.nome_contato asc";

		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $linha;
	}

	function getDadosContatoByIdBd($id_agenda){
		$sql = "SELECT a.id,a.nome_contato,agt.telefone,agm.email FROM agenda  a
				LEFT JOIN agenda_telefone agt on agt.id_agenda = a.id
				LEFT JOIN agenda_email agm on agm.id_agenda = a.id
				WHERE a.id = ".$id_agenda."
				ORDER BY a.nome_contato asc";
		$consulta = $this->conn->query($sql);		
		$linha = $consulta->fetch(PDO::FETCH_ASSOC);
		return $linha;
	}

	function excluirContatoByIdBd($id_agenda){
		$sql = "delete FROM agenda_telefone where id_agenda = ".$id_agenda;
		$this->conn->query($sql);		
	
		$sql = "delete FROM agenda_email where id_agenda = ".$id_agenda;
		$this->conn->query($sql);

		$sql = "delete FROM agenda where id = ".$id_agenda;
		return $this->conn->query($sql);
		
	}

}


?>
